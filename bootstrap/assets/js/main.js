(function($) {
    var originalH = 0;
    "use strict";
    $(window).on('scroll', function() {

        var y = $(window).scrollTop(),
            topBar = $('header');


        if (y > 1) {
            topBar.addClass('sticky');
            $('header .logo a').removeClass('logo-light');
            $('header .logo a').addClass('logo-dark');
            $('.main-navigation li.current a').addClass('current-sticky');
            $('.main-navigation li.highlight').addClass('sticky-sep');
            $('a.menu-toggle span').addClass('menu-open');
            $('.main-navigation li a').addClass('menu-open-main');


        } else {
            topBar.removeClass('sticky');
            $('header .logo a').removeClass('logo-dark');
            $('header .logo a').addClass('logo-light');
            $('.main-navigation li.current a').removeClass('current-sticky');
            $('.main-navigation li.highlight').removeClass('sticky-sep');
            $('.main-navigation li.highlight').addClass('with-sep');
            $('a.menu-toggle span').removeClass('menu-open');
            $('.main-navigation li a').removeClass('menu-open-main');

        }
    });
})(jQuery); // End of use strict




$(document).ready(function() {
    //Go to top button
    $("#go-top").on('click', function() {
        $('html, body').animate({ scrollTop: 0 }, 'slow');
    });
    //Counter Up
    $('.counter').counterUp({
        delay: 10,
        time: 1000
    });

    // Mobile Menu
    var toggleButton = $('.menu-toggle'),
        nav = $('.main-navigation-mobile');

    toggleButton.on('click', function(event) {
        event.preventDefault();

        toggleButton.toggleClass('is-clicked');
        nav.slideToggle();
    });

    if (toggleButton.is(':visible')) nav.addClass('mobile');

    $(window).resize(function() {
        if (toggleButton.is(':visible')) nav.addClass('mobile');
        else nav.removeClass('mobile');
    });

    $('#main-nav-wrap li a').on("click", function() {

        if (nav.hasClass('mobile')) {
            toggleButton.toggleClass('is-clicked');
            nav.fadeOut();
        }
    });

    // Offset for Main Navigation
    $('#mainNav').affix({
        offset: {
            top: 100
        }
    })

    //Carousel
    $("#bg-slider").owlCarousel({
        navigation: false, // Show next and prev buttons
        slideSpeed: 100,
        autoPlay: 5000,
        paginationSpeed: 100,
        singleItem: true,
        mouseDrag: false,
        transitionStyle: "fade"
    });

    //Accordion Faq
    $(document).on('show', '.accordion', function(e) {
        $(e.target).prev('.accordion-heading').addClass('accordion-opened');
    });

    $(document).on('hide', '.accordion', function(e) {
        $(this).find('.accordion-heading').not($(e.target)).removeClass('accordion-opened');
    });




    //Sortable Function for Faq
    var items = $('.item');
    items.show();
    $('.faq-filters li').on('click', function(e) {
        var category = $(this).data('filter');
        $(".clicked-filter").removeClass("clicked-filter");
        $(this).addClass("clicked-filter");
        //When user select 'All' show all items
        $('.item').each(function() {
            data = $(this).data('cat');
            if (data == category) {
                $(this).show();
            }
            if (data != category) {
                $(this).hide();
            }
            if (category == "*") {
                items.show();
            }
        });
        e.preventDefault();
    });

    //Clickable Map Appear
    if ($("#show-map").length > 0) {
        document.getElementById('show-map').onclick = function() {
            var map = document.getElementById('map-hidden');
            var opening = document.getElementById('show-map').className;
            if (map.className === "hide") {
                map.className = 'show';
                $('#map-hidden').addClass('animated fadeIn');
                document.getElementById('show-map').className = 'pe-7s-map-marker activa';
            }
            if (opening == "pe-7s-map-marker activa") {
                map.className = 'hide';
                document.getElementById('show-map').className = 'pe-7s-map-marker';
            }
        }
    }
    //Morphext
    $("#word-rotate").Morphext({
        animation: "fadeIn", // Overrides default "bounceIn"
        separator: ",", // Overrides default ","
        speed: 3500, // Overrides default 2000
        complete: function() {
            // Overrides default empty function
        }
    });

    //Typed Text
    $("#typed").typed({
        strings: ["Creative Multipurpose Software Template", "Design Templates with Usability.", "Showcase your product."],
        typeSpeed: 30,
        backDelay: 500,
        loop: true,
        // defaults to false for infinite loop
        loopCount: true,
        resetCallback: function() {
            newTyped();
        }
    });
    //Slider Hero
    $('.hero--slider').slick({
        autoplay: true,
        arrows: false,
        dots: true,
        fade: true,
        speed: 1000,
        cssEase: 'linear'
    });

    //Services Tabs
    $('.tab-content').not('.active').hide();

    $('.tab-nav a').on('click', function(e) {
        e.preventDefault();
        $('.tab-nav a').removeClass('active');
        $(this).addClass('active');

        $('.tab-content').hide();
        $($.attr(this, 'href')).fadeIn(300);
    });
    /* Video Tab Overlay */
    $('.video .overlay-video').on('click', function(ev) {
        $(this).fadeOut(300);
        $("#elvideo")[0].src += "&autoplay=1";
        ev.preventDefault();
    });

    //Archive Widget Blog

    $(".archive-first").on("click", function() {
        var obj = $(this).next();
        if ($(obj).hasClass("inactive-children")) {
            $(obj).removeClass("inactive-children").slideDown();
            $(this).find('.fa-plus').addClass('fa-minus').removeClass('fa-plus');
        } else {
            $(obj).addClass("inactive-children").slideUp();
            $(this).find('.fa-minus').addClass('fa-plus').removeClass('fa-minus');
        }
    });

    //Tooltip
    $("[data-toggle=tooltip]").tooltip({
        placement: 'right'
    });

    //Countdown
    $('#clock').countdown('2017/08/13', function(event) {
        var $this = $(this).html(event.strftime(''

            + '<span>%-D <p class="clock"> day%!D </p></span>' + '<span >%H <p class="clock"> hour%!D</p></span> ' + '<span>%M <p class="clock"> minute%!D</p></span> ' + '<span>%S <p class="clock"> second%!D</p></span>'));
    });

    //Placeholder Plugin Settings
    $('input, textarea, select, email, password').placeholder();
    originalH = document.getElementById("comparison").offsetHeight;

    /*Code For pricing page cards start*/
        resizeCardContainer();
        $(".show-or-hide-more-link").on("click",function(){
            if($('.show-or-hide-more-link').html().trim() == "see more...")
			{
				$(".card-hidden-content").show();
				var contentHeight = $($(".card-shown-content")[1]).height() + $($(".card-hidden-content")[1]).height() + $(".show-or-hide-more-link").height() + 30;
                var totalHeight = $(".card-header").height() + contentHeight + $(".card-footer").height() + 10;
                $(".card-content").css("height",contentHeight+"px");
                $(".card").css("height",totalHeight+"px");
                $(".price-comparision-card-container").css("height",(totalHeight+60)+"px");
                resizeCardContainer();
				$(".show-or-hide-more-link").html("see less...");
				//$(".Premium").css("margin-top","15px");
				//$(".Enterprise").css("margin-top","15px");
				
			}
			else
			{
				$(".card-hidden-content").hide();
                $(".show-or-hide-more-link").html("see more...");
                $(".card-content").css("height","auto");
                $(".card").css("height","auto");
                $(".price-comparision-card-container").css("height","100%");
                resizeCardContainer();
			}
			
            
			
        });
        $(".slider").on("click",function(){
            if($("#price-switch").is(":checked"))
            {
                $(".price-monthly").css("display","none");
                $(".price-annually").css("display","inline-block");
            }
            else
            {
                $(".price-monthly").css("display","inline-block");
                $(".price-annually").css("display","none");
            }
        });
    /*Code For pricing page cards start*/
});
function resizeCardContainer()
{
    $("#comparison").css("height",originalH+"px");
    var scrollH = document.getElementById("comparison").scrollHeight+25;
    $("#comparison").css("height",scrollH+"px");
}
/*Detailed Pricing*/
 
$('#price-link').click(function() {
  /* $('#price-list').toggle('slow', function() {
    // Animation complete.
  });*/
  $("#price-list").show();
});

$('#close-price').click(function() {
  $("#price-list").hide();
});

var priceArr = [{'countryAlpha':'A','countryImage':'Afghanistan.png','countryName':'Afghanistan','landline':'$0.376','mobile':'$0.358','tollFree':'-'},
				{'countryAlpha':'A','countryImage':'Albania.png','countryName':'Albania','landline':'$0.233','mobile':'$0.578','tollFree':'-'},
				{'countryAlpha':'A','countryImage':'Algeria.png','countryName':'Algeria','landline':'$0.109','mobile':'$0.400','tollFree':'-'},
				{'countryAlpha':'A','countryImage':'American_Samoa.png','countryName':'American Samoa','landline':'$0.054','mobile':'0.049€','tollFree':'-'},
				{'countryAlpha':'A','countryImage':'Andorra.png','countryName':'Andorra','landline':'$0.032','mobile':'$0.294','tollFree':'-'},
				{'countryAlpha':'A','countryImage':'Angola.png','countryName':'Angola','landline':'$0.130','mobile':'$0.159','tollFree':'-'},
				{'countryAlpha':'A','countryImage':'Anguilla.png','countryName':'Anguilla','landline':'$0.276','mobile':'$0.349','tollFree':'-'},
				{'countryAlpha':'A','countryImage':'Antarctica.png','countryName':'Antarctica','landline':'$2.213','mobile':'$2.213','tollFree':'-'},
				{'countryAlpha':'A','countryImage':'Antigua_and_Barbuda.png','countryName':'Antigua and Barbuda','landline':'$0.221','mobile':'$0.408','tollFree':'-'},
				{'countryAlpha':'A','countryImage':'Argentina.png','countryName':'Argentina','landline':'$0.013','mobile':'$0.037','tollFree':'$0.15'},
				{'countryAlpha':'A','countryImage':'Armenia.png','countryName':'Armenia','landline':'$0.176','mobile':'$0.263','tollFree':'-'},
				{'countryAlpha':'A','countryImage':'Aruba.png','countryName':'Aruba','landline':'$0.151','mobile':'$0.355','tollFree':'-'},
				{'countryAlpha':'A','countryImage':'Australia.png','countryName':'Australia','landline':'$0.027','mobile':'$0.034','tollFree':'$0.04'},
				{'countryAlpha':'A','countryImage':'Austria.png','countryName':'Austria','landline':'$0.019','mobile':'$0.038','tollFree':'$0.30'},
				{'countryAlpha':'A','countryImage':'Azerbaijan.png','countryName':'Azerbaijan','landline':'$0.368','mobile':'$0.582','tollFree':'-'},
				{'countryAlpha':'B','countryImage':'Bahamas.png','countryName':'Bahamas','landline':'$0.159','mobile':'$0.159','tollFree':'-'},
				{'countryAlpha':'B','countryImage':'Bahrain.png','countryName':'Bahrain','landline':'$0.139','mobile':'$0.119','tollFree':'-'},
				{'countryAlpha':'B','countryImage':'Bangladesh.png','countryName':'Bangladesh','landline':'$0.064','mobile':'$0.064','tollFree':'-'},
				{'countryAlpha':'B','countryImage':'Barbados.png','countryName':'Barbados','landline':'$0.320','mobile':'$0.417','tollFree':'-'},
				{'countryAlpha':'B','countryImage':'Belarus.png','countryName':'Belarus','landline':'$0.477','mobile':'$0.477','tollFree':'-'},
				{'countryAlpha':'B','countryImage':'Belgium.png','countryName':'Belgium','landline':'$0.039','mobile':'$0.039','tollFree':'$0.30'},
				{'countryAlpha':'B','countryImage':'Belize.png','countryName':'Belize','landline':'$0.380','mobile':'$0.406','tollFree':'-'},
				{'countryAlpha':'B','countryImage':'Benin.png','countryName':'Benin','landline':'$0.373','mobile':'$0.373','tollFree':'-'},
				{'countryAlpha':'B','countryImage':'Bermuda.png','countryName':'Bermuda','landline':'$0.063','mobile':'$0.063','tollFree':'-'},
				{'countryAlpha':'B','countryImage':'Bhutan.png','countryName':'Bhutan','landline':'$0.171','mobile':'$0.171','tollFree':'-'},
				{'countryAlpha':'B','countryImage':'Bolivia_Plurinational State_of.png','countryName':'Bolivia, Plurinational State of','landline':'$0.184','mobile':'$0.316','tollFree':'-'},
				{'countryAlpha':'B','countryImage':'Bosnia_and_Herzegovina.png','countryName':'Bosnia and Herzegovina','landline':'$0.215','mobile':'$0.506','tollFree':'-'},
				{'countryAlpha':'B','countryImage':'Botswana.png','countryName':'Botswana','landline':'$0.105','mobile':'$0.262','tollFree':'-'},
				{'countryAlpha':'B','countryImage':'Brazil.png','countryName':'Brazil','landline':'$0.019','mobile':'$0.202','tollFree':'$0.30'},
				{'countryAlpha':'B','countryImage':'Brunei_Darussalam.png','countryName':'Brunei Darussalam','landline':'$0.070','mobile':'$0.070','tollFree':'-'},
				{'countryAlpha':'B','countryImage':'Bulgaria.png','countryName':'Bulgaria','landline':'$0.062','mobile':'$0.011','tollFree':'-'},
				{'countryAlpha':'B','countryImage':'Burkina_Faso.png','countryName':'Burkina Faso','landline':'$0.378','mobile':'$0.537','tollFree':'-'},
				{'countryAlpha':'B','countryImage':'Burundi.png','countryName':'Burundi','landline':'$0.810','mobile':'$0.810','tollFree':'-'},
				{'countryAlpha':'C','countryImage':'Cambodia.png','countryName':'Cambodia','landline':'$0.100','mobile':'$0.100','tollFree':'-'},
				{'countryAlpha':'C','countryImage':'Cameroon.png','countryName':'Cameroon','landline':'$0.244','mobile':'$0.501','tollFree':'-'},
				{'countryAlpha':'C','countryImage':'Canada.png','countryName':'Canada','landline':'$0.012','mobile':'$0.012','tollFree':'$0.03'},
				{'countryAlpha':'C','countryImage':'Cape_Verde.png','countryName':'Cape Verde','landline':'$0.386','mobile':'$0.608','tollFree':'-'},
				{'countryAlpha':'C','countryImage':'Cayman_Islands.png','countryName':'Cayman Islands','landline':'$0.160','mobile':'$0.307','tollFree':'-'},
				{'countryAlpha':'C','countryImage':'Central_African_Republic.png','countryName':'Central African Republic','landline':'$0.524','mobile':'$0.519','tollFree':'-'},
				{'countryAlpha':'C','countryImage':'Chad.png','countryName':'Chad','landline':'$1.025','mobile':'$1.025','tollFree':'-'},
				{'countryAlpha':'C','countryImage':'Chile.png','countryName':'Chile','landline':'$0.030','mobile':'$0.130','tollFree':'-'},
				{'countryAlpha':'C','countryImage':'China.png','countryName':'China','landline':'$0.024','mobile':'$0.024','tollFree':'-'},
				{'countryAlpha':'C','countryImage':'Colombia.png','countryName':'Colombia','landline':'$0.033','mobile':'$0.067','tollFree':'$0.40'},
				{'countryAlpha':'C','countryImage':'Comoros.png','countryName':'Comoros','landline':'$0.485','mobile':'$0.582','tollFree':'-'},
				{'countryAlpha':'C','countryImage':'Congo.png','countryName':'Congo','landline':'$0.733','mobile':'$0.690','tollFree':'-'},
				{'countryAlpha':'C','countryImage':'Congo_The_Democratic_Republic_of_the.png','countryName':'Congo, The Democratic Republic of the','landline':'$0.487','mobile':'$0.383','tollFree':'-'},
				{'countryAlpha':'C','countryImage':'Cook_Islands.png','countryName':'Cook Islands','landline':'$1.324','mobile':'$1.324','tollFree':'-'},
				{'countryAlpha':'C','countryImage':'Costa_Rica.png','countryName':'Costa Rica','landline':'$0.027','mobile':'$0.078','tollFree':'-'},
				{'countryAlpha':'C','countryImage':'Cote_dIvoire.png','countryName':'Côte dIvoire','landline':'$0.512','mobile':'$0.356','tollFree':'-'},
				{'countryAlpha':'C','countryImage':'Croatia.png','countryName':'Croatia','landline':'$0.144','mobile':'$0.184','tollFree':'-'},
				{'countryAlpha':'C','countryImage':'Cuba.png','countryName':'Cuba','landline':'$1.265','mobile':'$1.265','tollFree':'-'},
				{'countryAlpha':'C','countryImage':'Cyprus.png','countryName':'Cyprus','landline':'$0.017','mobile':'$0.056','tollFree':'-'},
				{'countryAlpha':'C','countryImage':'Czech_Republic.png','countryName':'Czech Republic','landline':'$0.038','mobile':'$0.038','tollFree':'-'},
				{'countryAlpha':'D','countryImage':'Denmark.png','countryName':'Denmark','landline':'$0.018','mobile':'$0.037','tollFree':'-'},
				{'countryAlpha':'D','countryImage':'Djibouti.png','countryName':'Djibouti','landline':' $0.604€','mobile':'$0.604','tollFree':'-'},
				{'countryAlpha':'D','countryImage':'Dominica.png','countryName':'Dominica','landline':'$0.264','mobile':'$0.419','tollFree':'-'},
				{'countryAlpha':'D','countryImage':'Dominican_Republic.png','countryName':'Dominican Republic','landline':'$0.044','mobile':'$0.043','tollFree':'-'},
				{'countryAlpha':'E','countryImage':'Ecuador.png','countryName':'Ecuador','landline':'$0.180','mobile':'$0.202','tollFree':'-'},
				{'countryAlpha':'E','countryImage':'Egypt.png','countryName':'Egypt','landline':'$0.140','mobile':'$0.140','tollFree':'-'},
				{'countryAlpha':'E','countryImage':'El_Salvador.png','countryName':'El Salvador','landline':'$0.181','mobile':'$0.261','tollFree':'-'},
				{'countryAlpha':'E','countryImage':'Equatorial_Guinea.png','countryName':'Equatorial Guinea','landline':'$0.377','mobile':'$0.377','tollFree':'-'},
				{'countryAlpha':'E','countryImage':'Eritrea.png','countryName':'Eritrea','landline':'$0.434','mobile':'$0.432','tollFree':'-'},
				{'countryAlpha':'E','countryImage':'Estonia.png','countryName':'Estonia','landline':'$0.041','mobile':'$0.032','tollFree':'-'},
				{'countryAlpha':'E','countryImage':'Ethiopia.png','countryName':'Ethiopia','landline':'$0.423','mobile':'$0.423','tollFree':'-'},
				{'countryAlpha':'F','countryImage':'Falkland_Islands_(Malvinas).png','countryName':'Falkland Islands (Malvinas)','landline':'$2.854','mobile':'$2.854','tollFree':'-'},
				{'countryAlpha':'F','countryImage':'Faroe_Islands.png','countryName':'Faroe Islands','landline':'$0.096','mobile':'$0.306','tollFree':'-'},
				{'countryAlpha':'F','countryImage':'Fiji.png','countryName':'Fiji','landline':'$0.307','mobile':'$0.307','tollFree':'-'},
				{'countryAlpha':'F','countryImage':'Finland.png','countryName':'Finland','landline':'$0.082','mobile':'$0.094','tollFree':'-'},
				{'countryAlpha':'F','countryImage':'France.png','countryName':'France','landline':'$0.010','mobile':'$0.060','tollFree':'$0.08'},
				{'countryAlpha':'F','countryImage':'French_Guiana.png','countryName':'French Guiana','landline':'$0.038','mobile':'$0.038','tollFree':'-'},
				{'countryAlpha':'F','countryImage':'French_Polynesia.png','countryName':'French Polynesia','landline':'$0.445','mobile':'$0.512','tollFree':'-'},
				{'countryAlpha':'G','countryImage':'Gabon.png','countryName':'Gabon','landline':'$0.867','mobile':'$0.775','tollFree':'-'},
				{'countryAlpha':'G','countryImage':'Gambia.png','countryName':'Gambia','landline':'$0.847','mobile':'$0.847','tollFree':'-'},
				{'countryAlpha':'G','countryImage':'Georgia.png','countryName':'Georgia','landline':'$0.063','mobile':'$0.125','tollFree':'-'},
				{'countryAlpha':'G','countryImage':'Germany.png','countryName':'Germany','landline':'$0.020','mobile':'$0.061','tollFree':'$0.15'},
				{'countryAlpha':'G','countryImage':'Ghana.png','countryName':'Ghana','landline':'$0.264','mobile':'$0.354','tollFree':'-'},
				{'countryAlpha':'G','countryImage':'Gibraltar.png','countryName':'Gibraltar','landline':'$0.056','mobile':'$0.333','tollFree':'-'},
				{'countryAlpha':'G','countryImage':'Greece.png','countryName':'Greece','landline':'$0.023','mobile':'$0.045','tollFree':'-'},
				{'countryAlpha':'G','countryImage':'Greenland.png','countryName':'Greenland','landline':'$0.626','mobile':'$0.626','tollFree':'-'},
				{'countryAlpha':'G','countryImage':'Grenada.png','countryName':'Grenada','landline':'$0.354','mobile':'$0.411','tollFree':'-'},
				{'countryAlpha':'G','countryImage':'Guadeloupe.png','countryName':'Guadeloupe','landline':'$0.040','mobile':'$0.040','tollFree':'-'},
				{'countryAlpha':'G','countryImage':'Guam.png','countryName':'Guam','landline':'$0.040','mobile':'$0.040','tollFree':'-'},
				{'countryAlpha':'G','countryImage':'Guatemala.png','countryName':'Guatemala','landline':'$0.101','mobile':'$0.200','tollFree':'-'},
				{'countryAlpha':'G','countryImage':'Guernsey.png','countryName':'Guernsey','landline':'$0.027','mobile':'$0.027','tollFree':'-'},
				{'countryAlpha':'G','countryImage':'Guinea.png','countryName':'Guinea','landline':'$0.722','mobile':'$0.686','tollFree':'-'},
				{'countryAlpha':'G','countryImage':'Guinea_Bissau.png','countryName':'Guinea-Bissau','landline':'$0.580','mobile':'$0.565','tollFree':'-'},
				{'countryAlpha':'G','countryImage':'Guyana.png','countryName':'Guyana','landline':'$0.152','mobile':'$0.152','tollFree':'-'},
				{'countryAlpha':'H','countryImage':'Haiti.png','countryName':'Haiti','landline':'$0.445','mobile':'$0.445','tollFree':'-'},
				{'countryAlpha':'H','countryImage':'Holy See (Vatican City State).png','countryName':'Holy See (Vatican City State)','landline':'$17.745','mobile':'$17.745','tollFree':'-'},
				{'countryAlpha':'H','countryImage':'Honduras.png','countryName':'Honduras','landline':'$0.152','mobile':'$0.152','tollFree':'-'},
				{'countryAlpha':'H','countryImage':'Hong Kong.png','countryName':'Hong Kong','landline':'$0.027','mobile':'$0.049','tollFree':'-'},
				{'countryAlpha':'H','countryImage':'Hungary.png','countryName':'Hungary','landline':'$0.012','mobile':'$0.079','tollFree':'$0.15'},
				{'countryAlpha':'I','countryImage':'Iceland.png','countryName':'Iceland','landline':'$0.033','mobile':'$0.062','tollFree':'-'},
				{'countryAlpha':'I','countryImage':'India.png','countryName':'India','landline':'$0.046','mobile':'$0.046','tollFree':'-'},
				{'countryAlpha':'I','countryImage':'Indonesia.png','countryName':'Indonesia','landline':'$0.050','mobile':'$0.108','tollFree':'-'},
				{'countryAlpha':'I','countryImage':'Iran, Islamic Republic of.png','countryName':'Iran, Islamic Republic of','landline':'$0.139','mobile':'$0.207','tollFree':'-'},
				{'countryAlpha':'I','countryImage':'Iraq.png','countryName':'Iraq','landline':'$0.224','mobile':'$0.223','tollFree':'-'},
				{'countryAlpha':'I','countryImage':'Ireland.png','countryName':'Ireland','landline':'$0.016','mobile':'$0.084','tollFree':'-'},
				{'countryAlpha':'I','countryImage':'Isle of Man.png','countryName':'Isle of Man','landline':'$0.023','mobile':'$0.439','tollFree':'-'},
				{'countryAlpha':'I','countryImage':'Israel.png','countryName':'Israel','landline':'$0.022','mobile':'$0.030','tollFree':'-'},
				{'countryAlpha':'I','countryImage':'Italy.png','countryName':'Italy','landline':'$0.016','mobile':'$0.037','tollFree':'$0.30'},
				{'countryAlpha':'J','countryImage':'Jamaica.png','countryName':'Jamaica','landline':'$0.169','mobile':'$0.390','tollFree':'-'},
				{'countryAlpha':'J','countryImage':'Japan.png','countryName':'Japan','landline':'$0.050','mobile':'$0.184','tollFree':'-'},
				{'countryAlpha':'J','countryImage':'Jersey.png','countryName':'Jersey','landline':'$0.027','mobile':'$0.512','tollFree':'-'},
				{'countryAlpha':'J','countryImage':'Jordan.png','countryName':'Jordan','landline':'$0.147','mobile':'$0.147','tollFree':'-'},
				{'countryAlpha':'K','countryImage':'Kazakhstan.png','countryName':'Kazakhstan','landline':'$0.011','mobile':'$0.100','tollFree':'-'},
				{'countryAlpha':'K','countryImage':'Kenya.png','countryName':'Kenya','landline':'$0.178','mobile':'$0.176','tollFree':'-'},
				{'countryAlpha':'K','countryImage':'Kiribati.png','countryName':'Kiribati','landline':'$1.628','mobile':'$1.628','tollFree':'-'},
				{'countryAlpha':'K','countryImage':'Korea, Democratic Peoples Republic of.png','countryName':'Korea, Democratic Peoples Republic of','landline':'$0.861','mobile':'$0.861','tollFree':'-'},
				{'countryAlpha':'K','countryImage':'Korea, Republic of.png','countryName':'Korea, Republic of','landline':'$0.019','mobile':'$0.039','tollFree':'-'},
				{'countryAlpha':'K','countryImage':'Kuwait.png','countryName':'Kuwait','landline':'$0.094','mobile':'$0.147','tollFree':'-'},
				{'countryAlpha':'K','countryImage':'Kyrgyzstan.png','countryName':'Kyrgyzstan','landline':'$0.184','mobile':'$0.160','tollFree':'-'},
				{'countryAlpha':'L','countryImage':'Lao Peoples Democratic Republic.png','countryName':'Lao Peoples Democratic Republic','landline':'$0.113','mobile':'$0.113','tollFree':'-'},
				{'countryAlpha':'L','countryImage':'Latvia.png','countryName':'Latvia','landline':'$0.022','mobile':'$0.029','tollFree':'-'},
				{'countryAlpha':'L','countryImage':'Lebanon.png','countryName':'Lebanon','landline':'$0.162','mobile':'$0.317','tollFree':'-'},
				{'countryAlpha':'L','countryImage':'Lesotho.png','countryName':'Lesotho','landline':'$0.321','mobile':'$0.399','tollFree':'-'},
				{'countryAlpha':'L','countryImage':'Liberia.png','countryName':'Liberia','landline':'$0.633','mobile':'$0.672','tollFree':'-'},
				{'countryAlpha':'L','countryImage':'Libya.png','countryName':'Libya','landline':'$0.382','mobile':'$0.553','tollFree':'-'},
				{'countryAlpha':'L','countryImage':'Liechtenstein.png','countryName':'Liechtenstein','landline':'$0.142','mobile':'$0.323','tollFree':'-'},
				{'countryAlpha':'L','countryImage':'Lithuania.png','countryName':'Lithuania','landline':'$0.208','mobile':'$0.042','tollFree':'-'},
				{'countryAlpha':'L','countryImage':'Luxembourg.png','countryName':'Luxembourg','landline':'$0.028','mobile':'$0.242','tollFree':'-'},
				{'countryAlpha':'M','countryImage':'Macao.png','countryName':'Macao','landline':'$0.105','mobile':'$0.105','tollFree':'-'},
				{'countryAlpha':'M','countryImage':'Macedonia, Republic of.png','countryName':'Macedonia, Republic of','landline':'$0.294','mobile':'$0.730','tollFree':'-'},
				{'countryAlpha':'M','countryImage':'Madagascar.png','countryName':'Madagascar','landline':'$1.049','mobile':'$1.049','tollFree':'-'},
				{'countryAlpha':'M','countryImage':'Malawi.png','countryName':'Malawi','landline':'$0.572','mobile':'$0.615','tollFree':'-'},
				{'countryAlpha':'M','countryImage':'Malaysia.png','countryName':'Malaysia','landline':'$0.034','mobile':'$0.034','tollFree':'-'},
				{'countryAlpha':'M','countryImage':'Maldives.png','countryName':'Maldives','landline':'$1.495','mobile':'$1.473','tollFree':'-'},
				{'countryAlpha':'M','countryImage':'Mali.png','countryName':'Mali','landline':'$0.372','mobile':'$0.372','tollFree':'-'},
				{'countryAlpha':'M','countryImage':'Malta.png','countryName':'Malta','landline':'$0.023','mobile':'$0.070','tollFree':'-'},
				{'countryAlpha':'M','countryImage':'Marshall Islands.png','countryName':'Marshall Islands','landline':'$0.451','mobile':'$0.451','tollFree':'-'},
				{'countryAlpha':'M','countryImage':'Martinique.png','countryName':'Martinique','landline':'$0.039','mobile':'$0.038','tollFree':'-'},
				{'countryAlpha':'M','countryImage':'Mauritania.png','countryName':'Mauritania','landline':'$0.763','mobile':'$0.763','tollFree':'-'},
				{'countryAlpha':'M','countryImage':'Mauritius.png','countryName':'Mauritius','landline':'$0.126','mobile':'$0.126','tollFree':'-'},
				{'countryAlpha':'M','countryImage':'Mexico.png','countryName':'Mexico','landline':'$0.010','mobile':'$0.064','tollFree':'$0.08'},
				{'countryAlpha':'M','countryImage':'Micronesia, Federated States of.png','countryName':'Micronesia, Federated States of','landline':'$0.807','mobile':'$0.807','tollFree':'-'},
				{'countryAlpha':'M','countryImage':'Moldova, Republic of.png','countryName':'Moldova, Republic of','landline':'$0.236','mobile':'$0.317','tollFree':'-'},
				{'countryAlpha':'M','countryImage':'Monaco.png','countryName':'Monaco','landline':'$0.130','mobile':'$0.615','tollFree':'-'},
				{'countryAlpha':'M','countryImage':'Mongolia.png','countryName':'Mongolia','landline':'$0.061','mobile':'$0.061','tollFree':'-'},
				{'countryAlpha':'M','countryImage':'Montenegro.png','countryName':'Montenegro','landline':'$0.238','mobile':'$0.674','tollFree':'-'},
				{'countryAlpha':'M','countryImage':'Montserrat.png','countryName':'Montserrat','landline':'$0.444','mobile':'$0.444','tollFree':'-'},
				{'countryAlpha':'M','countryImage':'Morocco.png','countryName':'Morocco','landline':'$0.035','mobile':'$0.196','tollFree':'-'},
				{'countryAlpha':'M','countryImage':'Mozambique.png','countryName':'Mozambique','landline':'$0.124','mobile':'$0.178','tollFree':'-'},
				{'countryAlpha':'M','countryImage':'Myanmar.png','countryName':'Myanmar','landline':'$0.412','mobile':'$0.412','tollFree':'-'},
				{'countryAlpha':'N','countryImage':'Namibia.png','countryName':'Namibia','landline':'$0.113','mobile':'$0.208','tollFree':'-'},
				{'countryAlpha':'N','countryImage':'Nauru.png','countryName':'Nauru','landline':'$1.683','mobile':'$1.683','tollFree':'-'},
				{'countryAlpha':'N','countryImage':'Nepal.png','countryName':'Nepal','landline':'$0.196','mobile':'$0.196','tollFree':'-'},
				{'countryAlpha':'N','countryImage':'Netherlands.png','countryName':'Netherlands','landline':'$0.030','mobile':'$0.087','tollFree':'-'},
				{'countryAlpha':'N','countryImage':'New Caledonia.png','countryName':'New Caledonia','landline':'$0.313','mobile':'$0.357','tollFree':'-'},
				{'countryAlpha':'N','countryImage':'New Zealand.png','countryName':'New Zealand','landline':'$0.022','mobile':'$0.074','tollFree':'$0.15'},
				{'countryAlpha':'N','countryImage':'Nicaragua.png','countryName':'Nicaragua','landline':'$0.222','mobile':'$0.432','tollFree':'-'},
				{'countryAlpha':'N','countryImage':'Niger.png','countryName':'Niger','landline':'$0.578','mobile':'$0.578','tollFree':'-'},
				{'countryAlpha':'N','countryImage':'Nigeria.png','countryName':'Nigeria','landline':'$0.100','mobile':'$0.067','tollFree':'-'},
				{'countryAlpha':'N','countryImage':'Niue.png','countryName':'Niue','landline':'$1.318','mobile':'$1.318','tollFree':'-'},
				{'countryAlpha':'N','countryImage':'Northern Mariana Islands.png','countryName':'Northern Mariana Islands','landline':'$0.063','mobile':'$0.063','tollFree':'-'},
				{'countryAlpha':'N','countryImage':'Norway.png','countryName':'Norway','landline':'$0.011','mobile':'0.066€$0.073','tollFree':'-'},
				{'countryAlpha':'O','countryImage':'Oman.png','countryName':'Oman','landline':'$0.197','mobile':'$0.500','tollFree':'-'},
				{'countryAlpha':'P','countryImage':'Pakistan.png','countryName':'Pakistan','landline':'$0.204','mobile':'$0.204','tollFree':'-'},
				{'countryAlpha':'P','countryImage':'Palau.png','countryName':'Palau','landline':'$0.589','mobile':'$0.720','tollFree':'-'},
				{'countryAlpha':'P','countryImage':'Palestine, State of.png','countryName':'Palestine, State of','landline':'$0.280','mobile':'$0.321','tollFree':'-'},
				{'countryAlpha':'P','countryImage':'Panama.png','countryName':'Panama','landline':'$0.049','mobile':'$0.229','tollFree':'-'},
				{'countryAlpha':'P','countryImage':'Papua New Guinea.png','countryName':'Papua New Guinea','landline':'$1.130','mobile':'$1.130','tollFree':'-'},
				{'countryAlpha':'P','countryImage':'Paraguay.png','countryName':'Paraguay','landline':'$0.063','mobile':'$0.125','tollFree':'-'},
				{'countryAlpha':'P','countryImage':'Peru.png','countryName':'Peru','landline':'$0.016','mobile':'$0.092','tollFree':'-'},
				{'countryAlpha':'P','countryImage':'Philippines.png','countryName':'Philippines','landline':'$0.140','mobile':'$0.224','tollFree':'$0.40'},
				{'countryAlpha':'P','countryImage':'Pitcairn.png','countryName':'Pitcairn','landline':'$2.070','mobile':'$2.070','tollFree':'-'},
				{'countryAlpha':'P','countryImage':'Poland.png','countryName':'Poland','landline':'$0.014','mobile':'$0.027','tollFree':'-'},
				{'countryAlpha':'P','countryImage':'Portugal.png','countryName':'Portugal','landline':'$0.041','mobile':'$0.120','tollFree':'$0.30'},
				{'countryAlpha':'P','countryImage':'Puerto Rico.png','countryName':'Puerto Rico','landline':'$0.025','mobile':'$0.025','tollFree':'-'},
				{'countryAlpha':'Q','countryImage':'Qatar.png','countryName':'Qatar','landline':'$0.273','mobile':'$0.318','tollFree':'-'},
				{'countryAlpha':'R','countryImage':'Réunion.png','countryName':'Réunion','landline':'$0.165','mobile':'$0.182','tollFree':'-'},
				{'countryAlpha':'R','countryImage':'Romania.png','countryName':'Romania','landline':'$0.020','mobile':'$0.040','tollFree':'-'},
				{'countryAlpha':'R','countryImage':'Russian Federation.png','countryName':'Russian Federation','landline':'$0.023','mobile':'$0.043','tollFree':'-'},
				{'countryAlpha':'R','countryImage':'Rwanda.png','countryName':'Rwanda','landline':'$0.533','mobile':'$0.507','tollFree':'-'},
				{'countryAlpha':'S','countryImage':'Saint Barthélemy.png','countryName':'Saint Barthélemy','landline':'$0.040','mobile':'$0.040','tollFree':'-'},
				{'countryAlpha':'S','countryImage':'Saint Helena, Ascension and Tristan da Cunha.png','countryName':'Saint Helena, Ascension and Tristan da Cunha','landline':'$2.832','mobile':'$2.832','tollFree':'-'},
				{'countryAlpha':'S','countryImage':'Saint Kitts and Nevis.png','countryName':'Saint Kitts and Nevis','landline':'$0.220','mobile':'$0.419','tollFree':'-'},
				{'countryAlpha':'S','countryImage':'Saint Lucia.png','countryName':'Saint Lucia','landline':'$0.264','mobile':'$0.420','tollFree':'-'},
				{'countryAlpha':'S','countryImage':'Saint Pierre and Miquelon.png','countryName':'Saint Pierre and Miquelon','landline':'$0.149','mobile':'$0.149','tollFree':'-'},
				{'countryAlpha':'S','countryImage':'Saint Vincent and the Grenadines.png','countryName':'Saint Vincent and the Grenadines','landline':'$0.264','mobile':'$0.419','tollFree':'-'},
				{'countryAlpha':'S','countryImage':'Samoa.png','countryName':'Samoa','landline':'$1.086','mobile':'$1.086','tollFree':'-'},
				{'countryAlpha':'S','countryImage':'San Marino.png','countryName':'San Marino','landline':'$0.395','mobile':'$0.396','tollFree':'-'},
				{'countryAlpha':'S','countryImage':'Sao Tome and Principe.png','countryName':'Sao Tome and Principe','landline':'$2.328','mobile':'$2.328','tollFree':'-'},
				{'countryAlpha':'S','countryImage':'Saudi Arabia.png','countryName':'Saudi Arabia','landline':'$0.160','mobile':'$0.217','tollFree':'-'},
				{'countryAlpha':'S','countryImage':'Senegal.png','countryName':'Senegal','landline':'$0.312','mobile':'$0.462','tollFree':'-'},
				{'countryAlpha':'S','countryImage':'Serbia.png','countryName':'Serbia','landline':'$0.117','mobile':'$0.117','tollFree':'-'},
				{'countryAlpha':'S','countryImage':'Seychelles.png','countryName':'Seychelles','landline':'$0.722','mobile':'$0.722','tollFree':'-'},
				{'countryAlpha':'S','countryImage':'Sierra Leone.png','countryName':'Sierra Leone','landline':'$0.798','mobile':'$0.798','tollFree':'-'},
				{'countryAlpha':'S','countryImage':'Singapore.png','countryName':'Singapore','landline':'$0.036','mobile':'$0.019','tollFree':'-'},
				{'countryAlpha':'S','countryImage':'Slovakia.png','countryName':'Slovakia','landline':'$0.017','mobile':'$0.193','tollFree':'-'},
				{'countryAlpha':'S','countryImage':'Slovenia.png','countryName':'Slovenia','landline':'$0.045','mobile':'$0.045','tollFree':'-'},
				{'countryAlpha':'S','countryImage':'Solomon Islands.png','countryName':'Solomon Islands','landline':'$1.672','mobile':'$1.672','tollFree':'-'},
				{'countryAlpha':'S','countryImage':'South Africa.png','countryName':'South Africa','landline':'$0.588','mobile':'$0.588','tollFree':'-'},
				{'countryAlpha':'S','countryImage':'Somalia.png','countryName':'Somalia','landline':'$0.031','mobile':'$0.036','tollFree':'-'},
				{'countryAlpha':'S','countryImage':'Spain.png','countryName':'Spain','landline':'$0.018','mobile':'$0.020','tollFree':'$0.40'},
				{'countryAlpha':'S','countryImage':'Sri Lanka.png','countryName':'Sri Lanka','landline':'$0.212','mobile':'$0.204','tollFree':'-'},
				{'countryAlpha':'S','countryImage':'Sudan.png','countryName':'Sudan','landline':'$0.297','mobile':'$0.328','tollFree':'-'},
				{'countryAlpha':'S','countryImage':'Suriname.png','countryName':'Suriname','landline':'$0.208','mobile':'$0.291','tollFree':'-'},
				{'countryAlpha':'S','countryImage':'Swaziland.png','countryName':'Swaziland','landline':'$0.089','mobile':'$0.304','tollFree':'-'},
				{'countryAlpha':'S','countryImage':'Sweden.png','countryName':'Sweden','landline':'$0.011','mobile':'$0.024','tollFree':'$0.08'},
				{'countryAlpha':'S','countryImage':'Switzerland.png','countryName':'Switzerland','landline':'$0.017','mobile':'$0.470','tollFree':'-'},
				{'countryAlpha':'S','countryImage':'Syrian Arab Republic.png','countryName':'Syrian Arab Republic','landline':'$0.177','mobile':'$0.276','tollFree':'-'},
				{'countryAlpha':'T','countryImage':'Taiwan.png','countryName':'Taiwan','landline':'$0.031','mobile':'$0.184','tollFree':'-'},
				{'countryAlpha':'T','countryImage':'Tajikistan.png','countryName':'Tajikistan','landline':'$0.231','mobile':'$0.211','tollFree':'-'},
				{'countryAlpha':'T','countryImage':'Tanzania, United Republic of.png','countryName':'Tanzania, United Republic of','landline':'$0.560','mobile':'$0.560','tollFree':'-'},
				{'countryAlpha':'T','countryImage':'Thailand.png','countryName':'Thailand','landline':'$0.041','mobile':'$0.041','tollFree':'-'},
				{'countryAlpha':'T','countryImage':'Timor-Leste.png','countryName':'Timor-Leste','landline':'$1.608','mobile':'$1.608','tollFree':'-'},
				{'countryAlpha':'T','countryImage':'Togo.png','countryName':'Togo','landline':'$0.611','mobile':'$0.606','tollFree':'-'},
				{'countryAlpha':'T','countryImage':'Tokelau.png','countryName':'Tokelau','landline':'$2.103','mobile':'$2.103','tollFree':'-'},
				{'countryAlpha':'T','countryImage':'Tonga.png','countryName':'Tonga','landline':'$0.898','mobile':'$0.898','tollFree':'-'},
				{'countryAlpha':'T','countryImage':'Trinidad and Tobago.png','countryName':'Trinidad and Tobago','landline':'$0.074','mobile':'$0.287','tollFree':'-'},
				{'countryAlpha':'T','countryImage':'Tunisia.png','countryName':'Tunisia','landline':'$0.804','mobile':'$0.791','tollFree':'-'},
				{'countryAlpha':'T','countryImage':'Turkey.png','countryName':'Turkey','landline':'$0.062','mobile':'$0.239','tollFree':'-'},
				{'countryAlpha':'T','countryImage':'Turkmenistan.png','countryName':'Turkmenistan','landline':'$0.182','mobile':'$0.182','tollFree':'-'},
				{'countryAlpha':'T','countryImage':'Turks and Caicos Islands.png','countryName':'Turks and Caicos Islands','landline':'$0.276','mobile':'$0.398','tollFree':'-'},
				{'countryAlpha':'T','countryImage':'Tuvalu.png','countryName':'Tuvalu','landline':'$1.683','mobile':'$1.683','tollFree':'-'},
				{'countryAlpha':'U','countryImage':'Uganda.png','countryName':'Uganda','landline':'$0.531','mobile':'$0.531','tollFree':'-'},
				{'countryAlpha':'U','countryImage':'Ukraine.png','countryName':'Ukraine','landline':'$0.197','mobile':'$0.326','tollFree':'-'},
				{'countryAlpha':'U','countryImage':'United Arab Emirates.png','countryName':'United Arab Emirates','landline':'$0.234','mobile':'$0.234','tollFree':'-'},
				{'countryAlpha':'U','countryImage':'United Kingdom.png','countryName':'United Kingdom','landline':'$0.013','mobile':'$0.022','tollFree':'$0.08'},
				{'countryAlpha':'U','countryImage':'United States.png','countryName':'United States','landline':'$0.012','mobile':'$0.012','tollFree':'$0.03'},
				{'countryAlpha':'U','countryImage':'Uruguay.png','countryName':'Uruguay','landline':'$0.067','mobile':'$0.290','tollFree':'-'},
				{'countryAlpha':'U','countryImage':'Uzbekistan.png','countryName':'Uzbekistan','landline':'$0.036','mobile':'$0.081','tollFree':'-'},
				{'countryAlpha':'V','countryImage':'Vanuatu.png','countryName':'Vanuatu','landline':'$0.894','mobile':'$0.894','tollFree':'-'},
				{'countryAlpha':'V','countryImage':'Venezuela, Bolivarian Republic of.png','countryName':'Venezuela, Bolivarian Republic of','landline':'$0.011','mobile':'$0.131','tollFree':'-'},
				{'countryAlpha':'V','countryImage':'Viet Nam.png','countryName':'Viet Nam','landline':'$0.137','mobile':'$0.137','tollFree':'-'},
				{'countryAlpha':'V','countryImage':'Virgin Islands, British.png','countryName':'Virgin Islands, British','landline':'$0.327','mobile':'$0.419','tollFree':'-'},
				{'countryAlpha':'V','countryImage':'Virgin Islands, U.S.png','countryName':'Virgin Islands, British','landline':'$0.030','mobile':'$0.030','tollFree':'$0.03'},
				{'countryAlpha':'W','countryImage':'Wallis and Futuna.png','countryName':'Wallis and Futuna','landline':'$2.080','mobile':'$2.080','tollFree':'-'},
				{'countryAlpha':'Y','countryImage':'Yemen.png','countryName':'Yemen','landline':'$0.273','mobile':'$0.268','tollFree':'-'},
				{'countryAlpha':'Z','countryImage':'Zambia.png','countryName':'Zambia','landline':'$0.423','mobile':'$0.229','tollFree':'-'},
				{'countryAlpha':'Z','countryImage':'Zimbabwe.png','countryName':'Zimbabwe','landline':'$0.168','mobile':'$0.584','tollFree':'-'},
				];
var html = "";
var countryAlpha="";
var countryText="";
var imgSrc ="assets/img/countryimage/";
var priceHtml ="";
	priceHtml +='<div id="price-list" class="modal fade" role="dialog">';
	priceHtml +='	<div class="modal-dialog">';
	priceHtml +='		<div class="modal-content">';
	priceHtml +='			<div class="modal-header">';
	priceHtml +='				<button type="button" class="close" data-dismiss="modal">&times;</button>';
	priceHtml +='				<h3><span class="font-brand">Price per minute</span></h3>';
	priceHtml +='			</div>';
	priceHtml +='		</div>';
	priceHtml +='	</div>';
	priceHtml +='	<div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Close</button></div>';
	priceHtml +='	</div>';
	priceHtml +='</div>';
$(document).ready(function(){
	//$("body").append(priceHtml);
	//$(".modal-body" ).after(html);
	 $.each(priceArr,function(key,value){
		countryAlpha=value.countryAlpha;
		if(value.countryAlpha!=countryText){
			html += "<div class='row'><div class='col-xs-12 col-sm-12 col-md-12 col-lg-12' id='country-alpha'>"+countryAlpha+"</div></div>";
		}
		html += "<div class='row'>";
		html += "<div class='col-xs-2 col-sm-2 col-md-2 col-lg-2'><img src='"+imgSrc+value.countryImage+"'/></div>";
		html += "<div class='col-xs-3 col-sm-3 col-md-3 col-lg-3 country-text' id='country-text'>"+value.countryName+"</div>";
		html += "<div class='col-xs-2 col-sm-2 col-md-2 col-lg-2 landline'>"+value.landline+"</div>";
		html += "<div class='col-xs-2 col-sm-2 col-md-2 col-lg-2 mobile'>"+value.mobile+"</div>";
		html += "<div class='col-xs-2 col-sm-3 col-md-2 col-lg-2 tollfree'>"+value.tollFree+"</div>";
		html += "</div>";
		
		countryText=countryAlpha;
	});
	$(".price-table").html(html);
});
