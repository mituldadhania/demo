
<!DOCTYPE html>
<html lang="en" class="template-color-E37949">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta http-equiv="refresh" content="600; url=http://callhippo.com/" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <!-- <meta name="description" content="Buy local numbers in over 50 countries around the world. Receive and make calls from web. No desk phone, no complex setup. Get started within 60 seconds." /> -->
    <meta name="description" content=" The phone support software that offers Global Numbers to easily manage your phone support for SME business. Try CallHippo for Free. **Sign Up Now**" />
    <meta name="author" content="" />
    <meta name="p:domain_verify" content="b15a5d760b55224e3d6cb365a4c4a2e3"/>
    <link href="favicon.png" rel="shortcut icon" type="image/x-icon" />
    <title>Cloud based Virtual Phone Support Systems for SMB's - CallHippo</title>

    <!-- Bootstrap Core CSS -->
    <link href="assets/css/bootstrap.min.css?ver=1" rel="stylesheet" />
    <!-- Custom CSS -->
    <link href="assets/css/mainindex.css?ver=13" rel="stylesheet" />
    <link id="colors" href="assets/css/colors/orange.css?ver=1" rel="stylesheet" />
    <link rel="stylesheet" href="assets/css/responsive.css?ver=3" />
    <!-- Plugins CSS -->
    <link rel="stylesheet" href="assets/css/vendor.css" />
    <!-- Custom Fonts -->
    <link rel="stylesheet" href="assets/css/simple-line-icons.css" />
    <link rel="stylesheet" href="assets/css/pe-icon-7-stroke.css">
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,800|Montserrat:400,700">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
<script src="assets/js/html5shiv.js"></script>
<script src="assets/js/respond.min.js"></script>
<![endif]-->
<script>
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

 ga('create', 'UA-87436501-2', 'auto');
 ga('send', 'pageview');

</script>
</head>

<body>
    <!-- header 
================================================== -->
    <header>
        <div class="row-header">
            <div class="logo">
                <a href="/" class="logo-light"></a>
            </div>
            <nav id="main-nav-wrap">
                <ul class="main-navigation">
                   
                    <li><a class="smoothscroll" href="/features" title="">Features</a></li>
					 <li><a class="smoothscroll" href="/pricing" title="">Pricing</a></li>
                      
                  
                    <li><a class="smoothscroll" href="/blog" title="">Blog</a></li>
                   
                    <li class="highlight with-sep"><a href="https://dashboard.callhippo.com/login" title="">Sign In</a></li>
                    <li class="signup-btn"><a href="https://dashboard.callhippo.com/Signup" title="">Sign Up</a></li>
                </ul>
               
            </nav>
            <a class="menu-toggle" href="#"><span>Menu</span></a>
        </div>
    </header>
    <!-- /header -->
    <section id="hero" class="hero">
        <div id="img-back">
			<div id="videoDiv"> 
				<div id="videoBlock">
					<!--<div>
						<img src="http://www.imi21.com/wp-content/uploads/2016/11/t12.jpg" id="videosubstitute" alt="" width="800">
					</div>-->
					<video preload="preload" id="video" autoplay="autoplay" loop="loop">
						<source src="Converted_480.mp4" type="video/mp4"></source>
					</video>
					<div id="videoMessage">
						<div class="cnt-hero-content">
							<div class="hero-box">
								<div class="container">
									<div class="hero-text align-center">
										<h1>The Phone System For Super Efficient Teams</h1>
										<p>Get Local phone numbers for countries and cities around the world.</p>
									</div>
									<div class="action-main">
										<a href="https://dashboard.callhippo.com/Signup" class="no-decoration cnt-btn">SIGNUP FOR FREE TRIAL</a>
									</div>
								</div>
								<div class="wheel-wrap">
									<span class="mousewheel"></span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
            <!--<div class="cnt-hero-content">
                <div class="hero-box">
                    <div class="container">
                        <div class="hero-text align-center">
                            <h1>The Phone System For Super Efficient Teams</h1>
                            <p>Get Local phone numbers for countries and cities around the world.</p>
                        </div>
                        <div class="action-main">
                            <a href="https://dashboard.callhippo.com/Signup" class="no-decoration cnt-btn">SIGNUP FOR FREE TRIAL</a>
                        </div>
                    </div>
                    <div class="wheel-wrap">
                        <span class="mousewheel"></span>
                    </div>
                </div>
            </div>-->
            <!-- //.cnt-hero-bg -->
        </div>
        <!-- //.img-back -->
    </section>
    <section id="services">
        <div class="cnt-services-bg">
            <div class="container">
        
                <p class="title-txt">
Virtual phone system with amazing features</p>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-centered item-container">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 item-services">
                            <i class="pe-7s-call"></i>
                            <h2>Local Numbers</h2>
                            <p class="items">Get closer to your customers with local phone numbers from around the world.</p>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 item-services">
                            <i class="pe-7s-users"></i>
                            <h2>Assign Numbers To Each Team</h2>
                            <p class="items">With CallHippo you're able to assign numbers to a specific team in your support center. Track incoming, outgoing and missed calls to each number and team.</p>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 item-services">
                            <i class="pe-7s-rocket"></i>
                            <h2>Zero Complexity</h2>
                            <p class="items">Our User Interface, Price, Technology & Everything else are designed to inspire simplicity. So you don't have to waste time understanding the software.</p>
                        </div>
                    </div>
                    <!-- //.item-container -->
                    <div class="col-lg-12 action-started">
                        <a href="https://dashboard.callhippo.com/Signup" class="btn btn-outlined">Get started</a>
                    </div>
                </div>
                <!-- //.row -->
            </div>
            <!-- //.container-->
        </div>
        <!-- //.cnt-services-bg -->
    </section>
    <section id="features">
        <div class="cnt-features-bg">
            <div class="container">
            
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 item-cnt-feature">
                        <!-- Left -->
                        <div class="col-md-6 col-lg-6">
                            <!-- item init -->
                            <div class="col-md-12 col-lg-12 icon-box">
                                <div class="col-md-2 col-lg-2">
                                    <div class="icon">
                                        <i class="pe-7s-headphones"></i>
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-6 left-icon-text">
                                    <h2>Call From Any Number</h2>
                                    <p>Make calls from any local number over the world so you look local to your customers</p>
                                </div>
                            </div>
                            <!-- //item init -->
                            <!-- item init -->
                            <div class="col-md-12 col-lg-12 icon-box">
                                <div class="col-md-2 col-lg-2">
                                    <div class="icon">
                                        <i class="pe-7s-album"></i>
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-6 left-icon-text">
                                    <h2>Call Recording</h2>
                                    <p>Record calls on the number to monitor your teams performance</p>
                                </div>
                            </div>
                            <!-- //item init -->
                            <!-- item init -->
                            <div class="col-md-12 col-lg-12 icon-box">
                                <div class="col-md-2 col-lg-2">
                                    <div class="icon">
                                        <i class="pe-7s-hourglass"></i>
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-6 left-icon-text">
                                    <h2>Working Hours</h2>
                                    <p>Set custom working hours for each phone number you have and decide when to accept business calls</p>
                                </div>
                            </div>
                            <!-- //item init -->
                        </div>
                        <!-- //Left -->
                        <!-- Right -->
                        <div class="col-md-6 col-lg-6 side-img">
                            <img src="assets/img/db.png" alt="" class="img-responsive">
                        </div>
                        <!-- //Right -->
                    </div>
                </div>
                <div class="col-lg-12 action-started">
                    <a href="/features" class="btn btn-outlined">Find out more</a>
                </div>
            </div>
            <!-- //.container-->
        </div>
        <!-- //.cnt-features-bg -->
    </section>
    
    <section id="testimonials">
        <div class="cnt-testimonials-bg">
            <div class="container">
                <h1>Client Feedbacks</h1>
                <div class="row">
                    <div class="col-md-12 col-lg-12 items-testimonials">
                        <!-- item testimonial -->
                        <div class="col-md-4 col-lg-4 item-testimonial">
                            <div class="txt-testimonial">
                                <p>We needed a phone support system to answer our clients’ questions and reassure them. CallHippo was the easiest solution to implement for Office Hour.</p>
                            </div>
                            <div class="testimonial">
                                <img src="assets/img/yash.jpg" alt="" class="img-responsive" width="100" height="100">
                                <p class="name">Yash Shah</p>
                                <cite>CEO, Gridle</cite>
                            </div>
                        </div>
                        <!-- //item testimonial -->
                        <!-- item testimonial -->
                        <div class="col-md-4 col-lg-4 item-testimonial">
                            <div class="txt-testimonial">
                                <p>CallHippo is simple and easy to use. I could set it up in less than a minuteand had my team working on it less than 10 min.</p>
                            </div>
                            <div class="testimonial">
                                <img src="assets/img/jimit.png" alt="" class="img-responsive" width="100" height="100">
                                <p class="name">Jimit Bagadiya</p>
                                <cite>Founder, Social Pilot</cite>
                            </div>
                        </div>
                        <!-- //item testimonial -->
                        <!-- item testimonial -->
                        <div class="col-md-4 col-lg-4 item-testimonial">
                            <div class="txt-testimonial">
                                <p>We have team spread across multiple cities of the world. CallHippo is the simplest yet efficient calling system we could find.</p>
                            </div>
                            <div class="testimonial">
                                <img src="assets/img/chris.png" alt="" class="img-responsive" width="100" height="100">
                                <p class="name">Christopher Meloni</p>
                                <cite>Manager, DealsLand</cite>
                            </div>
                        </div>
                        <!-- //item testimonial -->
                    </div>
                </div>
            </div>
            <!-- //.container-->
        </div>
        <!-- //.cnt-testimonials-bg -->
    </section>
  
   
   
     <section id="investors">
        <div class="cnt-investors-bg">
            <div class="container">
           
                <h3 class="between-words">You're in a good company</h3>
                <div class="items-investors">
                    <!-- company 01 -->
                    <div class="col-xs-12 col-sm-3 left-hdr">
                        <div class="tab-first-company">
                            <img class="img-responsive company-style" src="http://www.oniva.de/images/logo.gif" alt="">
                        </div>
                    </div>
                    <!-- company 02 -->
                    <div class="col-xs-12 col-sm-3 left-hdr">
                        <div class="tab-second-company">
                            <img class="img-responsive company-style" src="http://www.esipick.com/themes/esipick-theme/assets/images/clients/2.png" alt="">
                        </div>
                    </div>
                    <!-- company 03 -->
                    <div class="col-xs-12 col-sm-3 left-hdr">
                        <div class="tab-third-company">
                            <img class="img-responsive company-style" src="https://www.wabion.ch/system/html/Logo_Bernegger_214px-4e237cb4.jpg" alt="">
                        </div>
                    </div>
                    <!-- company 04 -->
                    <div class="col-xs-12 col-sm-3 left-hdr">
                        <div class="tab-fourth-company">
                            <img class="img-responsive company-style" src="http://www.s-i-s.de/__we_thumbs__/1553_7_asklepios.jpg?m=1394134324" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <!-- //.container-->
        </div>
        <!-- //.cnt-investors-bg -->
    </section>
 
    <section id="started">
        <div class="cnt-started-bg">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <h2>Setup your support center within 3 minutes</h2>
                        <div class="col-lg-12 action-started">
                            <a href="https://dashboard.callhippo.com/Signup" class="btn btn-outlined">Get started</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- //.container-->
        </div>
        <!-- //.cnt-started-bg -->
    </section>
    <!-- footer
================================================== -->
    <footer>
        <div class="footer-main">
            <div class="row">
                <div class="col-md-4 col-lg-4 footer-info">
                    <div class="footer-logo"></div>
                   	COPYRIGHT © 2016 - CallHippo
                </div>
                <!-- /footer-info -->
                <div class="col-md-2 col-lg-2 site-links">
                    <h4>Company</h4>
                    <ul>
                   <li><a href="/about">About Us</a></li>
                        <li><a href="/blog">Blog</a></li>               
                        <li><a href="/privacy">Terms & Privacy</a></li>
                        <!-- <li><a href="https://callhippo.com/privacy">Privacy Policy</a></li> -->
                    </ul>
                </div>
                <!-- /site-links -->
                <div class="col-md-2 col-lg-2 social-links">
                    <h4>Social</h4>
                    <ul>
                        <li><a href="https://twitter.com/CallHippo">Twitter</a></li>
                        <li><a href="https://www.facebook.com/CallHippo">Facebook</a></li>
                    </ul>
                </div>
                <!-- /social -->
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 footer-subscribe">
                    <h5 class="contact-us-ft">Contact us</h5>
                   
                    <ul class="contact">
                        <li><i class="pe-7s-map-marker"></i> 340 S LEMON AVE #7359, WALNUT, CA 91789, UNITED STATES</li>
                        <li><i class="pe-7s-mail"></i> support@callhippo.com</li>
                    </ul>
                </div>
                <!-- /subscribe -->
            </div>
            <!-- /row -->
        </div>
        <!-- /footer-main -->
       
        <!-- /footer-bottom -->
    </footer>
	<!--
<script>
  (function() {
  var interakt = document.createElement('script');
  interakt.type = 'text/javascript'; interakt.async = true;
  interakt.src = "//cdn.interakt.co/interakt/a1bee60e9d5d3c1ffb976faebac06fd8.js";
  var scrpt = document.getElementsByTagName('script')[0];
  scrpt.parentNode.insertBefore(interakt, scrpt);
  })()
</script>   -->
<script>
	window.mySettings = {
	// TODO: The current logged in user's email address.
	email: '',
	// TODO: The current logged in user's sign-up date in Unix epoch time.
	created_at: '',
	app_id: 'a1bee60e9d5d3c1ffb976faebac06fd8',
	// Sending Custom Hash with unique_id
	purchase : {
	unique_id : '', //required for custom hashes otherwise the custom data is rejected // Unique String or Integer
	order_id : ''
	}
	};
</script>
    <!-- jQuery -->
    <script src="assets/js/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- Main -->
    <script src="assets/js/main.js?ver=3"></script>
    <!-- Plugins -->
    <script src="assets/js/plugins.js"></script>
    <!-- Google Map -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
    <script src="assets/js/map.js"></script>       
</body>

</html>
